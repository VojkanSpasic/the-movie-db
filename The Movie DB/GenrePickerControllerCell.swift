//
//  GenrePickerControllerCell.swift
//  The Movie DB
//
//  Created by Vojkan Spasic on 8/12/16.
//  Copyright © 2016 Vojkan Spasic. All rights reserved.
//

import UIKit

class GenrePickerControllerCell: UITableViewCell {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var genreTextField: UILabel!

}
