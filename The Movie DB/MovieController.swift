//
//  MovieController.swift
//  The Movie DB
//
//  Created by Vojkan Spasic on 7/30/16.
//  Copyright © 2016 Vojkan Spasic. All rights reserved.
//

import UIKit
import youtube_ios_player_helper
import CoreGraphics
import Foundation
import MBProgressHUD


class MovieController: UIViewController,UITableViewDelegate,UITableViewDataSource,YTPlayerViewDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var videoNameTextField: UITextField!
    @IBOutlet weak var releaseDateTextField: UITextField! // TO DO - SPELL MISTAKE
    @IBOutlet weak var overviewTextView: UITextView!
    @IBOutlet weak var overviewTextViewHightConstraint: NSLayoutConstraint!
    @IBOutlet weak var genresTextField: UITextField!
    
    var movie :Movie?
    var movieDetails:[MovieDetail]?
    var videoToLoad:MovieDetail?  // TO DO - CORECT NAME OF THIS VARIABLE
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationController?.toolbarHidden = true
        self.view.backgroundColor = UIColor.blackColor()
        tableView.backgroundColor = UIColor.blackColor()
        videoNameTextField.text = movie!.movieTitle
        releaseDateTextField.text = "("+movie!.releaseDate+")"
        overviewTextView.text = movie!.overview
        genresTextField.text = movie!.genreNames
        
        let sizeThatFitsTextView = overviewTextView.sizeThatFits(CGSizeMake(overviewTextView.frame.size.width, CGFloat(MAXFLOAT)))
        overviewTextViewHightConstraint.constant = sizeThatFitsTextView.height
        
        
        
        MovieDBManager.sharedInstance.fetchMovieVideos(forMovie: movie! ){ (movieDetails,error) in
            
            if let err = error{
                switch err{
                case.NetworkError:
                    dispatch_async(dispatch_get_main_queue(),{
                        let alert = UIAlertController(title: err.title, message: err.message, preferredStyle: .Alert)
                        let ok = UIAlertAction(title: "ok", style: .Default, handler: nil)
                        alert.addAction(ok)
                        self.presentViewController(alert, animated: true, completion: nil)
                    })
                case .MissingVideo:
                    dispatch_async(dispatch_get_main_queue(),{
                        let alert = UIAlertController(title: err.title, message: err.message, preferredStyle: .Alert)
                        let ok = UIAlertAction(title: "ok", style: .Default, handler: nil)
                        alert.addAction(ok)
                        self.presentViewController(alert, animated: true, completion: nil)
                    })
                }
            }
            if movieDetails != nil{
                self.movieDetails = movieDetails
            }
            self.tableView.reloadData()
        }
    }
    func playVideo(view:YTPlayerView,videoID:String){
        view.loadWithVideoId(videoID)
        let loader = MBProgressHUD.showHUDAddedTo(view, animated: true)
        loader.label.text = "Loading"
        loader.detailsLabel.text = "Plese vait"
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.movieDetails == nil{
            return 0
        }
        print(movieDetails)
        return self.movieDetails!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("CELL",forIndexPath: indexPath) as? MovieControllerCell
        videoToLoad = movieDetails![indexPath.row]
        cell?.trailerNameTextField.text = "Video:\(videoToLoad!.name)"
        cell?.trailerCountTextField.text = "\(indexPath.row+1) of \(movieDetails!.count)"
        return cell!
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = cell as? MovieControllerCell{
            let view = cell.playerView
            playVideo(view!, videoID: self.movieDetails![indexPath.row].youTubeKey)
        }
    }
}
