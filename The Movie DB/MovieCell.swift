//
//  MovieCell.swift
//  The Movie DB
//
//  Created by Vojkan Spasic on 8/3/16.
//  Copyright © 2016 Vojkan Spasic. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {

    @IBOutlet weak var posterView: UIImageView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var voteAverageLabel: UILabel!
    @IBOutlet weak var genresTextField: UITextField!
    @IBOutlet weak var previewTextView: UITextView!
    @IBOutlet weak var starImageView: UIImageView!
}

