//
//  MovieDB Menager.swift
//  The Movie DB
//
//  Created by Vojkan Spasic on 7/31/16.
//  Copyright © 2016 Vojkan Spasic. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

enum MovieDBMenagerError:ErrorType{
    case NetworkError(error:NSError)
    case MissingVideo(forMovie:Movie)
    
    var title:String?{
        switch self {
        case .NetworkError:
            return "Internet Connection Issue"
        case .MissingVideo:
            return "Missing video "
        }
    }
    var message:String?{
        switch self {
        case .NetworkError(let realError):
            return realError.localizedDescription + "Please check your network connection or try again later"
        case.MissingVideo(let movie):
            return "Video for movie " + movie.movieTitle + " is not available please pick another movie"
        }
    }
}


class MovieDBManager{
    
    static let sharedInstance = MovieDBManager()
    var selectedMovieId : String = ""
    var selctedGenreId : String = ""
    let apiKey = "1a8cf68cea1be9ce3938eb5a6024d19a"
    
    func fetchGenre(result:(genres:[Genre]?,error:MovieDBMenagerError?) -> Void){
        
        Alamofire.request(.GET,"https://api.themoviedb.org/3/genre/movie/list?api_key=1a8cf68cea1be9ce3938eb5a6024d19a").validate().responseJSON { (response) in
            switch response.result{
                
            case .Success:
                if let value = response.result.value{
                    let json = JSON(value)
                    var genres:[Genre]=[]
                    let resolt = json["genres"].arrayValue
                    for item in resolt{
                        let genreName = item["name"].stringValue
                        let genreId = item["id"].stringValue
                        let genre = Genre(genreId:genreId,genreName: genreName)
                        genres.append(genre)
                    }
                    result(genres:genres, error: nil)
                    
                }
            case .Failure(let error):
                result(genres:nil, error: MovieDBMenagerError.NetworkError(error: error))
                print (error.localizedDescription)
            }
        }
    }
    
    
    
    
    func fetchMoviePage(page:Double,with genreId:String,result:(movies:[Movie]?,error:MovieDBMenagerError?)->Void){
        
        var movies :[Movie] = []
        let movieByGenre = "https://api.themoviedb.org/3/discover/movie?api_key=1a8cf68cea1be9ce3938eb5a6024d19a&append_to_response=images&page=\(page)&include_video&with_genres=\(genreId)"
        
        // get list of movies of specific genre with video
        Alamofire.request(.GET,movieByGenre).validate().responseJSON { (response) in
            switch response.result{
            case .Success:
                if let value = response.result.value{
                    let json = JSON(value)
                    print("JSON: \(json)")
                    for item in json["results"].arrayValue{
                        
                        let movieTitle = item["original_title"].stringValue
                        let movieId = item["id"].stringValue
                        let moviePosterPath = item["poster_path"].stringValue
                        let voteAverage = round(1000*item["vote_average"].doubleValue)/1000
                        let overview = item["overview"].stringValue
                        let realiseDate = item["release_date"].stringValue
                        let genreIds = item["genre_ids"].arrayValue
                        var stringGenreIds = [String]()
                        for genreId in genreIds{
                            let stringGenreId = genreId.stringValue
                            stringGenreIds.append(stringGenreId)
                        }
                        let movie = Movie(movieTitle: movieTitle,
                            movieID: movieId,
                            youTubeKey: nil,
                            voteAverage: voteAverage,
                            overview: overview,
                            releaseDate: realiseDate,
                            genreIds: stringGenreIds,
                            moviePosterPath: "https://image.tmdb.org/t/p/w342\(moviePosterPath)")
                        movies.append(movie)
                        
                    }
                    result(movies: movies, error: nil)
                    
                }
            case .Failure(let error):
                
                result(movies: nil, error: MovieDBMenagerError.NetworkError(error: error))
                print (error.localizedDescription)
            }
        }
    }
    
    func fetchMovieVideos(forMovie movie:Movie,resolt:(movieDetails:[MovieDetail]?,error:MovieDBMenagerError?)->Void){
        
        var movieDetails:[MovieDetail]=[]
        Alamofire.request(.GET,"https://api.themoviedb.org/3/movie/\(movie.movieID)/videos?api_key=1a8cf68cea1be9ce3938eb5a6024d19a").validate().responseJSON { (response) in
            switch response.result{
                
            case .Success:
                if let value = response.result.value{
                    let json = JSON(value)
                    print(json)
                    if json["results"] == []{
                        resolt(movieDetails: nil, error: MovieDBMenagerError.MissingVideo(forMovie: movie))
                        return
                    }
                    
                    for videoDetails in json["results"].arrayValue{
                        let youTubeKey = videoDetails["key"].stringValue
                        let name = videoDetails["name"].stringValue
                        let movieDetail = MovieDetail(name:name,youTubeKey:youTubeKey)
                        movieDetails.append(movieDetail)
                        resolt(movieDetails: movieDetails,error: nil)
                    }
                }
            case .Failure(let error):
                resolt(movieDetails:nil,error:MovieDBMenagerError.NetworkError(error: error))
                
            }
        }
    }
}
