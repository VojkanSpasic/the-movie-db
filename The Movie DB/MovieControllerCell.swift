//
//  MovieControllerCell.swift
//  The Movie DB
//
//  Created by Vojkan Spasic on 8/7/16.
//  Copyright © 2016 Vojkan Spasic. All rights reserved.
//

import UIKit
import youtube_ios_player_helper
import MBProgressHUD

class MovieControllerCell: UITableViewCell,YTPlayerViewDelegate{
    
    
    @IBOutlet weak var playerView: YTPlayerView!
    @IBOutlet weak var trailerCountTextField: UITextField!
    @IBOutlet weak var trailerNameTextField: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        playerView.delegate = self
        self.playerView.backgroundColor = UIColor.blackColor()
        self.playerView.webView?.backgroundColor = UIColor.blackColor()
    }
    
    
    func playerViewPreferredWebViewBackgroundColor(playerView: YTPlayerView) -> UIColor {
        let color = UIColor.blackColor()
        return color
    }
    func playerViewPreferredInitialLoadingView(playerView: YTPlayerView) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.blackColor()
        return view
    }
    func playerViewDidBecomeReady(playerView: YTPlayerView) {
        MBProgressHUD.hideHUDForView(playerView, animated: true)
    }
    
    
    
}
