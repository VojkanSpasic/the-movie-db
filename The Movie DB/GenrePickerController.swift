//
//  ViewController.swift
//  The Movie DB
//
//  Created by Vojkan Spasic on 7/28/16.
//  Copyright © 2016 Vojkan Spasic. All rights reserved.

import UIKit
import Foundation
import SwiftyJSON
import CoreGraphics
import MBProgressHUD


class GenrePickerController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    var genres:[Genre]?
    var selectedGenreID:String!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.shouldAutorotate()
        
        self.navigationItem.titleView?.contentMode = .ScaleAspectFit
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        self.navigationController?.navigationBar.backgroundColor = UIColor.blackColor()
        self.navigationController?.navigationBar.barTintColor = UIColor.blackColor()
        self.navigationController?.toolbarHidden = true
        
        let loader = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        MovieDBManager.sharedInstance.fetchGenre { (genres, error) in
            if let err = error{
                dispatch_async(dispatch_get_main_queue(),{
                    let alert = UIAlertController(title: err.title, message: err.message, preferredStyle: .Alert)
                    let ok = UIAlertAction(title: "ok", style: .Default, handler: nil)
                    alert.addAction(ok)
                    self.presentViewController(alert, animated: true, completion: nil)
                })
            }
            if genres != nil{
                self.genres = genres
                
            }
            loader.hideAnimated(true)
            self.tableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("MEMORY WARNING!!!")
    }
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if genres == nil{
            return 0
        }else{
            return (genres?.count)!
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CELL",forIndexPath: indexPath) as? GenrePickerControllerCell
        cell?.genreTextField.text = genres![indexPath.row].genreName
        cell?.logoImageView.image = UIImage(named: "Image")
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let moviePickerController = segue.destinationViewController as? MoviePickerController{
            if let idIndex = tableView.indexPathForSelectedRow?.row{
                tableView.deselectRowAtIndexPath(tableView.indexPathForSelectedRow!, animated: true)
                moviePickerController.selectedGenre = genres![idIndex]
                if  moviePickerController.movies != nil{
                    moviePickerController.tableView.reloadData()
                }
            }
        }
    }
    @IBAction func backToGenres(segue:UIStoryboardSegue){
        if segue.identifier == "backToGenres"{
            let moviePickerController = segue.sourceViewController as! MoviePickerController
            moviePickerController.currentPage = 1
            self.navigationController?.toolbarHidden = true
        }
    }
}

