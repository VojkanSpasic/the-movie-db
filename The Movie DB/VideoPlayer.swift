//
//  VideoPlayer.swift
//  The Movie DB
//
//  Created by Vojkan Spasic on 7/30/16.
//  Copyright © 2016 Vojkan Spasic. All rights reserved.
//

import UIKit


class VideoPlayer: UIViewController{
    
    @IBOutlet var webView:UIWebView!
    var urlPath = "https://www.youtube.com/watch?v=Rva9ylPHi2w"
    
    func loadAdressUrl(){
    
        let requestURL = NSURL(string:urlPath)
        let request = NSURLRequest(URL: requestURL!)
        webView.loadRequest(request)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadAdressUrl()
        

      
     
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
