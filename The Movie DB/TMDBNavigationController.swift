//
//  TMDBNavigationController.swift
//  The Movie DB
//
//  Created by Vojkan Spasic on 8/7/16.
//  Copyright © 2016 Vojkan Spasic. All rights reserved.
//

import UIKit

class TMDBNavigationController: UINavigationController {
    
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
}
