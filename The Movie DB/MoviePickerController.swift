//
//  MoviePickerController.swift
//  The Movie DB
//
//  Created by Vojkan Spasic on 7/29/16.
//  Copyright © 2016 Vojkan Spasic. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SwiftyJSON
import CoreGraphics
import MBProgressHUD
import AlamofireImage
import CCBottomRefreshControl



class MoviePickerController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var selectedGenre:Genre?
    var movies:[Movie]?
    var currentPage:Double = 1
    
    var loader:MBProgressHUD?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundColor = UIColor.blackColor()
        
        let refreshControle = UIRefreshControl()
        refreshControle.triggerVerticalOffset = 100
        refreshControle.addTarget(self, action: #selector(refresh(_:)), forControlEvents: .ValueChanged)
        self.tableView.bottomRefreshControl = refreshControle
        
        
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        
        loader = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loader!.backgroundView.color = UIColor.blackColor()
        loader!.label.text = "Loading"
        loader!.detailsLabel.text = "Please wait"
        
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        
        self.tableView.backgroundColor = UIColor.blackColor()
        
        let manager = MovieDBManager()
        manager.fetchMoviePage(1,with:selectedGenre!.genreId) { (movies, error) in
            
            if let err = error{
                dispatch_async(dispatch_get_main_queue(),{
                    let alert = UIAlertController(title: err.title, message: err.message, preferredStyle: .Alert)
                    let ok = UIAlertAction(title: "ok", style: .Default, handler: nil)
                    alert.addAction(ok)
                    self.presentViewController(alert, animated: true, completion: nil)
                    self.loader!.hideAnimated(true)
                })
            }else{
                self.movies = movies
                self.loader!.hideAnimated(true)
                self.tableView.reloadData()
            }
        }
    }
    
    func refresh(refreshControl: UIRefreshControl) {
        loadNextPage()
        refreshControl.endRefreshing()
    }
    
    
    func loadNextPage(){
        
        currentPage += 1
        let manager = MovieDBManager()
        
        manager.fetchMoviePage(currentPage,with: selectedGenre!.genreId) { (movies, error) in
            
            if movies != nil{
                self.movies? += movies!
            }else {
                print(error)
            }
            self.tableView.reloadData()
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.movies == nil {
            return 0
        }else{
            return self.movies!.count
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("CELL") as! MovieCell
        let URL = movies![indexPath.row].moviePosterUrl
        cell.starImageView.translatesAutoresizingMaskIntoConstraints = false
        cell.starImageView.image = UIImage(named:"white star")
        cell.posterView.af_setImageWithURL(URL!, placeholderImage:UIImage(named: "tmdb logo"))
        cell.titleTextField.text = movies![indexPath.row].movieTitle
        cell.releaseDateLabel.text = movies![indexPath.row].releaseDate
        cell.genresTextField.text = movies![indexPath.row].genreNames
        cell.voteAverageLabel.text = String(movies![indexPath.row].voteAverage)
        cell.previewTextView.text = movies![indexPath.row].overview
        return cell
        
    }
    
    
    
    
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let movieController = segue.destinationViewController as? MovieController{
            if let index = tableView.indexPathForSelectedRow?.row{
                tableView.deselectRowAtIndexPath(tableView.indexPathForSelectedRow!, animated: true)
                let movieID = movies![index].movieID
                movieController.movie = movies![index]
                MovieDBManager.sharedInstance.selectedMovieId = movieID
            }
        }
    }
    
    @IBAction func backToMovies(segue:UIStoryboardSegue){
        if segue.identifier == "backToMovies"{
            let moviePickerController = segue.destinationViewController as? MoviePickerController
            moviePickerController?.navigationController?.toolbarHidden = false
        }
    }
}
